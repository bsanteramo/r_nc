args <- commandArgs(trailingOnly = TRUE)
print(args)

j <- as.integer(args[1])

print(j)


# open input files #
DP = read.table("inputFile/DP.txt", sep="\t" )
PI = read.table("inputFile/PI.txt", sep="\t" )
PJ = read.table("inputFile/PJ.txt", sep="\t" )

# import libraries
library(tsDyn) 
library(zoo)


# initialize
DP=zoo(DP)
dim(DP)

obs<-dim(DP)[1]
draws<-dim(DP)[2]


# script
Seosummary<-array("NA",c(1,draws)) ;  ## Create a vector of 1000 observations "NA"
SeoPvalue <-array("NA",c(1,draws)) ;  ## Create a vector of 1000 observations "NA"
SeoSupWald<-array("NA",c(1,draws)) ;  ## Create a vector of 1000 observations "NA"

Prices<-array("NA",c(obs,2))       ;  ## Create a 2 vectors of 1000 observations "NA"


###############################################################
#   This is a loop that compute a Seo test.                   #
#   You can set the number of bootstras (replications)        #
#   by changing nboot from 1 to, say, 200.                    #
#   "Seo" will collect the information and we extract the     #
#   useful information and collect in                         #
#   SeoPvalue , SeoSupWald and Seosummary                     #
#                                                             #
#   To start, set "for (j in (1:2))" and nboot=1              #
###############################################################

################################
## Run the function in parallel:
## Not run:
#we show here the use with package doMC
library(doMC)
registerDoMC() #Number of cores
################################

    Prices<-cbind(PI[,j],PJ[,j])	
    Seo<-try(TVECM.SeoTest(Prices,lag=1,beta=1,nboot=1000, hpc=c("foreach")))
    SeoPvalue[j]<-Seo[5]
    SeoSupWald[j]<-Seo[1]

 
#    if (Seo[5]<0.05)  {    
#    Seosummary[j]<-0
#        }        else		{
#    Seosummary[j]<-1
#        }
    print(j)

#Seosummary


#write.table(Seosummary,file="outputFile/Seosummary.csv",sep=",",row.names = TRUE)
#write.table(SeoPvalue,file="outputFile/SeoPvalue.csv",sep=",",row.names = TRUE)
#write.table(SeoSupWald,file="outputFile/SeoSupWald.csv",sep=",",row.names = TRUE)

result<-array("NA",c(1,2)) ;
result[1]<-SeoPvalue[j]
result[2]<-SeoSupWald[j]
write.table(result,file="result.csv",sep=",",row.names = FALSE)
