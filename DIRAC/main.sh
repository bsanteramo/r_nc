start_total_time=$(date '+%s')

echo ">> number of cores <<" >> log
cat /proc/cpuinfo | grep "processor" | wc -l >> log
echo "<< number of cores >>" >> log
echo
echo "**** extracting archive ****"
start_extract_time=$(date '+%s')
tar -xvzf R1.tar.gz
end_extract_time=$(date '+%s')
echo "****************************"
echo
echo "**** installing R ****"
start_install_time=$(date '+%s')
bash INSTALL.sh 2>&1
end_install_time=$(date '+%s')
echo "**********************"
echo
echo "**** create inputFile folder ****"
mkdir inputFile
echo "*********************************"
echo
echo "**** moving input files in inputFile folder ****"
mv DP.txt inputFile/
mv PI.txt inputFile/
mv PJ.txt inputFile/
echo "*************************************************"
echo 
echo "**** create outputFile folder ****"
mkdir outputFile/
echo "*********************************"
echo
echo "**** launching simulation ****"
start_simulation_time=$(date '+%s')
bash launch.sh $@
end_simulation_time=$(date '+%s')
echo "******************************"
echo
echo "**** moving output files ****"
mv outputFile/SeoPvalue.csv outputFile/SeoPvalue_$@.csv
mv outputFile/SeoSupWald.csv outputFile/SeoSupWald_$@.csv
mv outputFile/Seosummary.csv outputFile/Seosummary_$@.csv
echo "*****************************"
echo
echo "**** creating symbolic links for output files ****"
ln -s outputFile/SeoPvalue_$@.csv
ln -s outputFile/SeoSupWald_$@.csv
ln -s outputFile/Seosummary_$@.csv
echo "**************************************************"
echo
echo "**** listing folder ****"
ls -la
echo "************************"
echo
echo "**** listing inputFile folder ****"
ls -la inputFile/
echo "**********************************"
echo
echo "**** listing outputFile folder ****"
ls -la outputFile/
echo "**********************************"

end_total_time=$(date '+%s')

echo ">> extract time <<" >> log
extract_time=$((end_extract_time - start_extract_time))
echo $extract_time >> log
echo "<< extract time >>" >> log
echo ">> install time <<" >> log
install_time=$((end_install_time - start_install_time))
echo $install_time >> log
echo "<< install time >>" >> log
echo ">> simulation time <<" >> log
simulation_time=$((end_simulation_time - start_simulation_time))
echo $simulation_time >> log
echo "<< simulation time >>" >> log
echo ">> total time <<" >> log
total_time=$((end_total_time - start_total_time))
echo $total_time >> log
echo "<< total time >>" >> log

if [ -f result.csv ]
then
    echo result.csv exists
    exit 0
else
    echo result.csv does not exist
    exit 1
fi
