TIMEFORMAT="%E %U %S"

#source set_variables

time R --vanilla < hansen.r --args $@

mv test1vs2.csv ~/workspace/R_NC/scenarios/4/output/draws/test1vs2.$@.csv

mv test1vs3.csv ~/workspace/R_NC/scenarios/4/output/draws/test1vs3.$@.csv

mv test2vs3.csv ~/workspace/R_NC/scenarios/4/output/draws/test2vs3.$@.csv
