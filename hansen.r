args <- commandArgs(trailingOnly = TRUE)
print(args)

j <- as.integer(args[1])

print(j)


# open input files #
#DP = read.table("inputFile/DP.txt", sep="\t" )

DP = read.table("scenarios/4/input/DP.txt", sep="\t" )

# import libraries
library(tsDyn)
library(zoo)


# initialize
DP=zoo(DP)
dim(DP)

obs<-dim(DP)[1]
draws<-dim(DP)[2]

test1vs2  <-array("NA",c(1,draws));
test1vs3  <-array("NA",c(1,draws));
test2vs3  <-array("NA",c(1,draws));

################################
## Run the function in parallel:
## Not run:
#we show here the use with package doMC
#library(doMC)
#registerDoMC(8) #Number of cores
################################

#for (j in (1:draws)) 
#for (j in (1:1))
#for (j in (j:j))
#  {
  Hansentest<- try(setarTest(DP[,j],  m=1, thDelay=0, nboot=1000,trim=0.10, test="1vs"))
#  Hansentest<- try(setarTest(DP[,j],  m=1, thDelay=0, nboot=1000, trim=0.10, test="1vs", hpc="foreach"))
  test1vs2[1,j]<-try(Hansentest$PvalBoot[1]) 
  test1vs3[1,j]<-try(Hansentest$PvalBoot[2]) 
#  }

#for (j in (1:draws)) 
#for (j in (1:1))
#for (j in (j:j))
#  {
  Hansentest<- try(setarTest(DP[,j],  m=1, thDelay=0, nboot=1000,trim=0.10, test="2vs3"))
#  Hansentest<- try(setarTest(DP[,j],  m=1, thDelay=0, nboot=1000, trim=0.10, test="2vs3", hpc="foreach"))
  test2vs3[1,j]<-try(Hansentest$PvalBoot) 
#  }


write.table(test1vs2,file="test1vs2.csv",sep=",",row.names = FALSE)
write.table(test1vs3,file="test1vs3.csv",sep=",",row.names = FALSE)
write.table(test2vs3,file="test2vs3.csv",sep=",",row.names = FALSE)
