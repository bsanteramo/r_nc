TIMEFORMAT="%E %U %S"

#source set_variables

time R --vanilla < seo_test.r --args $@

mv result.csv ~/workspace/R_NC/scenarios/1/seo_test_output/draws/result.$@.csv

mv seo.out ~/workspace/R_NC/scenarios/1/seo_test_output/draws/seo.$@.out
