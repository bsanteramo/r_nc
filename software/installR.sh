echo "**** extracting archive ****"
start_extract_time=$(date '+%s')
tar -xvzf R.tar.gz
end_extract_time=$(date '+%s')
echo "****************************"
echo
echo "**** installing R ****"
start_install_time=$(date '+%s')
bash INSTALL.sh 2>&1
end_install_time=$(date '+%s')
echo "**********************"

echo ">> extract time <<" >> log
extract_time=$((end_extract_time - start_extract_time))
echo $extract_time >> log
echo "<< extract time >>" >> log
echo ">> install time <<" >> log
install_time=$((end_install_time - start_install_time))
echo $install_time >> log
echo "<< install time >>" >> log
