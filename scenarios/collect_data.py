#!/usr/bin/python

import os
import sys
import linecache

# get base directory
base_dir = os.getcwd()
#print base_dir # debug

scenario = sys.argv[1]
#print scenario # debug

draws_dir = base_dir + "/" + scenario + "/" + "output" + "/" + "draws"
#print draws_dir # debug

header = ""
test1vs2 = ""
test1vs3 = ""
test2vs3 = ""

def draw_check(draw_file):
  if not os.path.exists(draw_file):
    print draw_file + " NOT EXISTS"
    sys.exit()

#def make_header():
#  for i in range(1,1001):
#    header = header + "\"V" + i + "\","
#  print header # debug

for i in range(1,1001):
  header = header + "\"V" + str(i) + "\","
header = header[:-1]
#print header # debug


for draw in range(1,1001):
#  print draw # debug
  draw_file = draws_dir + "/" + "test1vs2." + str(draw) + ".csv"
#  print draw_file # debug
  draw_check(draw_file)
  line_data = linecache.getline(draw_file, 2)
#  print line_data # debug
  draw_data = line_data.split(",")[draw - 1]
#  print draw_data # debug
  test1vs2 = test1vs2 + "," + draw_data
#  print test1vs2 # debug

  draw_file = draws_dir + "/" + "test1vs3." + str(draw) + ".csv"
#  print draw_file # debug
  draw_check(draw_file)
  line_data = linecache.getline(draw_file, 2)
  draw_data = line_data.split(",")[draw - 1]
  test1vs3 = test1vs3 + "," + draw_data

  draw_file = draws_dir + "/" + "test2vs3." + str(draw) + ".csv"
#  print draw_file # debug
  draw_check(draw_file)
  line_data = linecache.getline(draw_file, 2)
  draw_data = line_data.split(",")[draw - 1]
  test2vs3 = test2vs3 + "," + draw_data

output_dir = base_dir + "/" + scenario + "/" + "output" + "/"

test1vs2 = test1vs2[1:-1]
test1vs2_file = output_dir + "test1vs2.csv"
f = open(test1vs2_file, 'w')
f.write(header + "\n")
f.write(test1vs2 + "\n")
f.close()

test1vs3 = test1vs3[1:-1]
test1vs3_file = output_dir + "test1vs3.csv"
f = open(test1vs3_file, 'w')
f.write(header + "\n")
f.write(test1vs3 + "\n")
f.close()

test2vs3 = test2vs3[1:-1]
test2vs3_file = output_dir + "test2vs3.csv"
f = open(test2vs3_file, 'w')
f.write(header + "\n")
f.write(test2vs3 + "\n")
f.close()

print "WORK TERMINATED"
